/*
 * Copyright (c) 2016-2019, Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 *
 */

#ifndef SDC600_TYPES_H_
#define SDC600_TYPES_H_

#include "stdint.h"

typedef enum bool_t {
    false,
    true,
} bool_t;

#endif /* SDC600_TYPES_H_ */
